import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino: DestinoViaje;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>

  //@Input => Indica que la variable puede ser pasada como parametro en otro componente

  constructor() {
    //this.nombre = 'Pablo Roca';
    this.clicked = new EventEmitter();
   }

  ngOnInit(): void {
  }

  ir(): boolean{
    this.clicked.emit(this.destino);
    return false;
  }

}
